
    // Calculate body force source term
    forAll(vgObjects,vgI)
    {
        vgObjects[vgI]->calcLift(lift,mesh,U,nu);
    }

    // Momentum predictor
    tmp<fvVectorMatrix> UEqn
    (
        fvm::div(phi, U)
        + turbulence->divDevReff(U)
        ==
        fvOptions(U) - lift
    );

    UEqn().relax();

    fvOptions.constrain(UEqn());

    solve
    (
	UEqn() == -fvc::grad(p)
    );

    fvOptions.correct(U);
