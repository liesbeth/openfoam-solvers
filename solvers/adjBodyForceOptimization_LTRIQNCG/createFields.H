// * SET DICTIONARIES * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

IOdictionary transportProperties
(
    IOobject
    (
        "transportProperties",
        runTime.constant(),
        runTime,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    )
);

IOdictionary vgProperties
(
    IOobject
    (
        "vgProperties",
        runTime.constant(),
        runTime,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    )
);
dictionary vgDict = vgProperties.subDict("vgDict");
wordList vgNames = vgDict.toc();
Info << "VG's in system:" << endl << vgNames << endl;
dictionary vgSolutionDict = vgProperties.subDict("vgSolutionDict");

IOdictionary adjointDict
(
    IOobject
    (
        "adjointDict",
        runTime.constant(),
        runTime,
        IOobject::MUST_READ_IF_MODIFIED,
        IOobject::NO_WRITE
    )
);


// * READ PRIMAL AND DUAL FIELD VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * 

// read primal variables
Info << "Reading kinematic viscosity nu\n" << endl;
dimensionedScalar nu(transportProperties.lookup("nu"));

Info<< "Reading field p\n" << endl;
volScalarField p
(
    IOobject
    (
        "p",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

Info<< "Reading field U\n" << endl;
volVectorField U
(
    IOobject
    (
        "U",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

#include "createPhi.H"

label pRefCell = 0;
scalar pRefValue = 0.0;
setRefCell
(
    p, 
    mesh.solutionDict().subDict("SIMPLE"), 
    pRefCell, 
    pRefValue
 );


// read adjoint field variables
Info<< "Reading field pa\n" << endl;
volScalarField pa
(
    IOobject
    (
        "pa",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

Info<< "Reading field Ua\n" << endl;
volVectorField Ua
(
    IOobject
    (
        "Ua",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

#include "createPhia.H"

label paRefCell = 0;
scalar paRefValue = 0.0;
setRefCell
(
    pa,
    mesh.solutionDict().subDict("SIMPLE"),
    paRefCell,
    paRefValue
);

   
// turbulence model
singlePhaseTransportModel laminarTransport(U, phi);

autoPtr<incompressible::RASModel> turbulence
(
    incompressible::RASModel::New(U, phi, laminarTransport)
);

// create fields to store previous converged flow field, to restore if TR metric < 0
#include "createPrevVars.H"


// * ADJOINT AND COST FUNCTION PARAMETERS * * * * * * * * * * * * * * * * * * * * * * * * 

//read objective velocity field
Info << "Reading field UObj\n" << endl;
volVectorField UObj
(
    IOobject
    (
        "UObj",
        "objective",
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    ),
    mesh
);

// Initialize source term adj. eq. due to cost function
volVectorField dU
(
    IOobject
    (
        "dU",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    U-UObj
);

// Read vgCells, jCells and vgForceIni (obtained from adjCellSelection utility)
volVectorField vgForceIni
(
    IOobject
    (
        "vgForceIni",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    ),
    mesh
);

volScalarField vgCells
(
    IOobject
    (
        "vgCells",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    ),
    mesh
);
scalar Vtot = fvc::domainIntegrate(neg(-vgCells)).value();

volScalarField jCells
(
    IOobject
    (
        "jCells",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::NO_WRITE
    ),
    mesh
);


// create list with cell labels of vgCells
labelList fLabels;
fLabels.clear();

label nProcessors = Pstream::nProcs();

int vgProc = 0;
if (Pstream::master())
{
  vgProc = -1;
}
Pstream::scatter(vgProc,Pstream::blocking);

for (int i = 0; i < nProcessors; i++)
{
    if (Pstream::myProcNo() == i)
    {
        labelList subLabels;
        subLabels.clear();

        forAll(mesh.C(),celli)
        {
            if (vgCells[celli] > 0)
            {
                subLabels.append(celli);
            }
        }
        if (subLabels.size() != 0)
        {
            vgProc = i;
            Pout << "Found vgCells on proc " << vgProc << endl;
            fLabels.append(subLabels);
        }
    }
}

int Ncells = fLabels.size();

Info << "Cells in body force domain = " << Ncells << endl;


// Adjust a force component if desired
vector adjustIniForce(adjointDict.lookupOrDefault<vector>("adjustIniForce",vector(0,0,0) ));

if (adjustIniForce.component(0) == 1)
{
    scalar Fx;
    adjointDict.lookup("Fx") >> Fx;
    forAll(fLabels,labeli)
    {
        vgForceIni[fLabels[labeli]].component(0) = Fx / Vtot;
    }
}
if (adjustIniForce.component(1) == 1)
{
    scalar Fy;
    adjointDict.lookup("Fy") >> Fy;
    forAll(fLabels,labeli)
    {
        vgForceIni[fLabels[labeli]].component(1) = Fy / Vtot;
    }
}
if (adjustIniForce.component(2) == 1)
{
    scalar Fz;
    adjointDict.lookup("Fz") >> Fz;
    forAll(fLabels,labeli)
    {
        vgForceIni[fLabels[labeli]].component(2) = Fz / Vtot;
    }
}


// Find neighbours of vgCells to construct averaging filter
bool averagingFilter(adjointDict.lookupOrDefault<bool>("averagingFilter",true));
scalar filterRelaxation(adjointDict.lookupOrDefault<scalar>("filterRelaxation",1));

RectangularMatrix <label> averagingCells(3*Ncells,7,0);
RectangularMatrix <scalar> NaveragingCells(3*Ncells,1,0);
RectangularMatrix <scalar> VaveragingCells(3*Ncells,1,0);
RectangularMatrix <scalar> VvgCells(3*Ncells,1,0);
RectangularMatrix <scalar> weightVector(7,1,1);

    int sum = 0;
    scalar vol = 0;
    labelList neighbours;

    for (int i = 0; i < fLabels.size(); i++)
    {
        averagingCells[3*i][0] = 3*i;
        averagingCells[3*i+1][0] = 3*i+1;
        averagingCells[3*i+2][0] = 3*i+2;

        VvgCells[3*i][0] = mesh.V()[fLabels[i]];
        VvgCells[3*i+1][0] = mesh.V()[fLabels[i]];
        VvgCells[3*i+2][0] = mesh.V()[fLabels[i]];

        sum = 1;
        vol = mesh.V()[fLabels[i]];
        neighbours = mesh.cellCells()[fLabels[i]];

        for (int n = 0; n < neighbours.size(); n++)
        {
            for (int j = 0; j < fLabels.size(); j++)
            {
                if (fLabels[j] == neighbours[n])
                {
                    sum++;
                    averagingCells[3*i][sum-1] = 3*j;
                    averagingCells[3*i+1][sum-1] = 3*j+1;
                    averagingCells[3*i+2][sum-1] = 3*j+2;

                    vol = vol + mesh.V()[fLabels[j]];
                    VvgCells[3*i][sum-1] = mesh.V()[fLabels[j]];
                    VvgCells[3*i+1][sum-1] = mesh.V()[fLabels[j]];
                    VvgCells[3*i+2][sum-1] = mesh.V()[fLabels[j]];
                    j = fLabels.size();
                }
            }
        }

        NaveragingCells[3*i][0] = sum;
        NaveragingCells[3*i+1][0] = sum;
        NaveragingCells[3*i+2][0] = sum;

        VaveragingCells[3*i][0] = vol;
        VaveragingCells[3*i+1][0] = vol;
        VaveragingCells[3*i+2][0] = vol;
    }


// Set VG body force field
volVectorField vgForce
(
    IOobject
    (
        "vgForce",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    vgForceIni
);
vector Ftot0(fvc::domainIntegrate(neg((vgCells-2))*vgForce).value());
vector Ftot(Ftot0);


// Initialize objective functional, gradient and Hessian
Info << "Initialization objective function..." << endl;
dimensionedScalar alphaW(adjointDict.lookup("alphaW"));

dimensionedScalar objFnc("J",dimless,1);
dimensionedScalar objFncPrev("Jprev",dimless,0);
dimensionedScalar dJ(objFnc - objFncPrev);
dJ = 1;

// initialize control parameters = Cf
Info << "Initialization control parameters in Cf..." << endl;

scalar CfInitial(adjointDict.lookupOrDefault<scalar>("CfInitial",1));
scalar Cfav(CfInitial);

RectangularMatrix <scalar> Cf(3*Ncells,1,CfInitial);
RectangularMatrix <scalar> Cftemp(3*Ncells,1,CfInitial);
RectangularMatrix <scalar> Cfprev(3*Ncells,1,CfInitial);
RectangularMatrix <scalar> pk(3*Ncells,1,0);

RectangularMatrix <scalar> gradObjFnc(3*Ncells,1,1);
RectangularMatrix <scalar> gradObjFncPrev(3*Ncells,1,1);
RectangularMatrix <scalar> dgradObjFnc(3*Ncells,1,0);

RectangularMatrix <scalar> unitMatrix(3*Ncells,3*Ncells,0);
for (int i = 0; i < 3*Ncells; i++)
{
    unitMatrix[i][i] = 1;
}
RectangularMatrix <scalar> Hessian(unitMatrix);
RectangularMatrix <scalar> HessianPrev(unitMatrix);
RectangularMatrix <scalar> HessianInv(unitMatrix);
RectangularMatrix <scalar> HessianInvPrev(unitMatrix);
RectangularMatrix <scalar> yk(3*Ncells,1,0);

volVectorField gradJ
(
    IOobject
    (
        "gradJ",
        runTime.timeName(),
        mesh,
        IOobject::READ_IF_PRESENT,
        IOobject::AUTO_WRITE
    ),
    mesh,
    Foam::vector(0,0,0)
);


RectangularMatrix <scalar> unitVec(3*Ncells,1,1);
scalar gradJ_norm = (gradObjFnc.T()*unitVec)[0][0];


// Reinitialize gradObjFnc in case gradJ is read from file (restart)
if (gradJ_norm != 0)
{
    forAll(fLabels,labeli)
    {
        gradObjFnc[3*labeli][0] = gradJ[fLabels[labeli]].component(0);
        gradObjFnc[3*labeli+1][0] = gradJ[fLabels[labeli]].component(1);
        gradObjFnc[3*labeli+2][0] = gradJ[fLabels[labeli]].component(2);
    }
}


// * OPTIMIZATION PARAMETERS * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

// General parameters
word optMethod(adjointDict.lookup("optMethod"));
word resetMethod(adjointDict.lookup("resetMethod"));
word Hmethod("L-BFGS");

scalar pk_norm = 1;
gradJ_norm = 1;
scalar eps = 1;

scalar tol(adjointDict.lookupOrDefault<scalar>("tolerance_Cf",1e-5));
scalar tolJ(adjointDict.lookupOrDefault<scalar>("tolerance_J",1e-6));
scalar tol_dJ(adjointDict.lookupOrDefault<scalar>("tolerance_dJ",tolJ/10));
scalar tolgrad(adjointDict.lookupOrDefault<scalar>("tolerance_gradJ",1e-10));

int iteration(0);
int maxIt(adjointDict.lookupOrDefault<scalar>("maxIter",25));
int outerit(0);
int maxOuterIt(adjointDict.lookupOrDefault<scalar>("maxOuterIter",10));

scalar simpleStartTime(0);
scalar maxSimpleIterations(runTime.endTime().value());


// Low memory BFGS variables
scalar m(adjointDict.lookupOrDefault<scalar>("BFGSsteps",5));

RectangularMatrix <scalar> Sm(0,0,0);
RectangularMatrix <scalar> Ym(0,0,0);
RectangularMatrix <scalar> Am(2*m,2*m,0);
RectangularMatrix <scalar> Lm(0,0,0);
RectangularMatrix <scalar> Dm(0,0,0);

RectangularMatrix <scalar> Stemp(0,0,0);
RectangularMatrix <scalar> Ytemp(0,0,0);

scalar gk = 0;


// Trust region variables and matrices
scalar delta(adjointDict.lookupOrDefault<scalar>("delta",1));
scalar deltaprev(delta);
scalar deltaUp(adjointDict.lookupOrDefault<scalar>("deltaUp",2));
scalar deltaDown(adjointDict.lookupOrDefault<scalar>("deltaDown",3));
scalar deltaMax(adjointDict.lookupOrDefault<scalar>("deltaMax",100));
scalar TR_n(adjointDict.lookupOrDefault<scalar>("TR_n",1e-4));

scalar deltaJumpMin(adjointDict.lookupOrDefault<scalar>("deltaJumpMin",10));
scalar deltaJumpMax(adjointDict.lookupOrDefault<scalar>("deltaJumpMax",25));
scalar deltaJumpStep = deltaJumpMax - deltaJumpMin;
scalar factorF = 1;

scalar eps_TR1 = 0.25;
scalar eps_TR2 = 0.75;
scalar eps_TR3 = 1.25;

dimensionedScalar metric(objFnc);
scalar tk = 1;
scalar tk1 = 0;
scalar tk2 = 0;
bool foundLocalMin = false;

scalar test1 = 1;
scalar test2 = 1;

RectangularMatrix <scalar> CfValues(Cf.n(),maxOuterIt,0);
RectangularMatrix <scalar> JValues(2,maxOuterIt,0);

RectangularMatrix <scalar> pku(3*Ncells,1,0);
RectangularMatrix <scalar> pkb(3*Ncells,1,0);

scalar doglegA = 0;
scalar doglegB = 0;
scalar doglegC = 0;

scalar check = (pk.T()*yk)[0][0];

bool reinitialized = false;



const labelList& inletCells = mesh.boundary()["inlet"].faceCells();


   
